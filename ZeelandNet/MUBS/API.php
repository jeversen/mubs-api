<?php

namespace ZeelandNet\MUBS;

use GuzzleHttp\Client;
use ZeelandNet\MUBS\Services\ProductGroupService;
use ZeelandNet\MUBS\Services\ProductDetailService;
use ZeelandNet\MUBS\Services\TokenService;

class API
{
    private $client;
    private $productGroupService;
    private $productDetailService;
    private $tokenService;
    private $devBaseUrl = "http://bestelo.deltaportals.nl";
    private $baseUrl = "https://klantworden.delta.nl";

    public function __construct($devMode = false)
    {
        if (!defined("MUBS_BASE_URL")) {
            define("MUBS_BASE_URL", $devMode ? $this->devBaseUrl : $this->baseUrl);
        }
        $this->client = new Client(['cookies' => true,  /*'proxy' => "localhost:8888",*/ 'verify' => false]);
        $this->tokenService = new TokenService($this->client);
        $this->productGroupService = new ProductGroupService($this->client, $this->tokenService);
        $this->productDetailService = new ProductDetailService($this->client, $this->tokenService);
    }

    public function getProductGroups(string $zipCode, int $houseNumber, ?string $housenumberAddition)
    {
        return $this->productGroupService->getProductGroupsForAddress($zipCode, $houseNumber, $housenumberAddition);
    }

    public function getProductGroupsAsJSON(string $zipCode, int $houseNumber, ?string $housenumberAddition)
    {
        return json_encode($this->getProductGroups($zipCode, $houseNumber, $housenumberAddition), JSON_PRETTY_PRINT);
    }

    public function getProductDetails(int $conversionPathGroupId, array $addressInfo)
    {
        return $this->productDetailService->getProductDetailsForConversionPathGroupId($conversionPathGroupId, $addressInfo);
    }
}
