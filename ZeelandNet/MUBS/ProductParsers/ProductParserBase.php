<?php

namespace ZeelandNet\MUBS\ProductParsers;

use PHPHtmlParser\Dom;

interface IProductParser
{
    public function getProducts() : array;
}

abstract class ProductParserBase implements IProductParser
{
    protected $dom;
    protected $deeplink = "http://bestelo.deltaportals.nl/bestellen/check/";

    public function __construct(string $html, array $addressInfo)
    {
        $this->deeplink .= str_replace(" ", "", $addressInfo['zipCode']) . "," . $addressInfo['houseNumber'] . ",,true/";
        
        $this->dom = new Dom();
        $this->dom->load($html);
    }
}
