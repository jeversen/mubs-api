<?php

namespace ZeelandNet\MUBS\ProductParsers;

class AllInOneParser extends ProductParserBase
{
    public function getProducts() : array
    {
        $products = $this->dom->find("#AllInOne .item")->toArray();

        return array_map(
            function ($product) {
                $presentableProduct = [];
                $presentableProduct['image'] = $product->find('.productImage')[0]->find('img')->getAttribute('src');
                $presentableProduct['image'] = MUBS_BASE_URL . $presentableProduct['image'];
                $presentableProduct['key'] = $product->find('.deal')[0]->getAttribute('data-reference');
                $presentableProduct['title'] = $product->find(".js-offer-category")[0]->innerHtml();
                $presentableProduct['subtitle'] = $product->find(".AllInOneWidgetTemplate")[0]->innerHtml();
                $presentableProduct['price'] =  $product->find(".amount")[0]->innerHtml();
                $presentableProduct['real_price'] =  $product->find(".pricesubtitle")[0]->innerHtml();
                $presentableProduct['deeplink'] = $this->deeplink . $presentableProduct['key'];
                
                return $presentableProduct;
            },
            $products
        );
    }
}
