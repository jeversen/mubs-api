<?php

namespace ZeelandNet\MUBS\Services;

use PHPHtmlParser\Dom;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;

class ProductGroupService
{
    private $productGroupServiceUrl = MUBS_BASE_URL . "/api/aanbod/producten";
    
    private $productGroupFormData = [
        "__RequestVerificationToken" => null,
        "StepNumber" => 1,
        "ZipCode" => null,
        "HouseNumber" => null,
        "HouseNumberAddition" => "",
        "IsHomeOrWorkAddress" => true,
        "SimulationDateEnergy" => "03-04-2018",
        "SimulationDateMultiMedia" => "03-04-2018",
        "OpenConversionPathGroup" => "",
        "CustomerAllreadyLivesAtAddress" => true,
    ];

    private $client;
    private $tokenService;
    private $objectFromWebservice;

    public function __construct(\GuzzleHttp\Client $client, TokenService $tokenService)
    {
        $this->client = $client;
        $this->tokenService = $tokenService;
    }

    public function getObjectFromWebservice()
    {
        return $this->objectFromWebservice;
    }

    private function prepareFormData(string $zipCode, int $houseNumber, ?string $houseNumberAddition) {
        $this->productGroupFormData['__RequestVerificationToken'] = $this->tokenService->getProductGroupToken();
        $this->productGroupFormData['ZipCode'] = $zipCode;
        $this->productGroupFormData['HouseNumber'] =  $houseNumber;
        $this->productGroupFormData['HouseNumberAddition'] = $houseNumberAddition;
    }

    public function getProductGroupsForAddress(string $zipCode, int $houseNumber, ?string $houseNumberAddition)
    {
        $this->prepareFormData($zipCode, $houseNumber, $houseNumberAddition);
        $this->objectFromWebservice = json_decode($this->makeServiceCallAndReturnJsonResult());
        
        return  [
                    'addressInfo'   => (array)$this->objectFromWebservice->addressInfo,
                    'productGroups' => $this->getProductGroupsFromHtml($this->objectFromWebservice->serviceAvailabilityHtmlResult)
                ];
    }

    private function makeServiceCallAndReturnJsonResult() : string
    {
        return $this->client
                    ->request("POST", $this->productGroupServiceUrl, [ "form_params" => $this->productGroupFormData ])
                    ->getBody();
    }

    private function getProductGroupsFromHtml(string $html) : array
    {
        $dom = new Dom();
        $dom->load($html);
        $productGroups = $dom->find("#conversionPathGroupsMenu .group-selection-bar")->toArray();

        return array_map(
            function ($product) {
                $o = [];
                $o['image'] = str_replace("/rbs", "", $product->find(".group-image")[0]->find("img")->getAttribute('src'));
                $o['image'] = MUBS_BASE_URL . $o['image'];
                $o['title'] = $product->find(".group-title")[0]->innerHtml();
                $o['text'] = $product->find(".group-text")[0]->innerHtml();
                $o['conversionPathId'] = $product->find(".group-anchor")[0]->getAttribute('data-id');
                return $o;
            },
            $productGroups
        );
    }
}
