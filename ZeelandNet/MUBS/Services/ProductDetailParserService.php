<?php

namespace ZeelandNet\MUBS\Services;

class ProductDetailParserService
{
    private $conversionPathGroupIdParserMap =
    [
        1 => 'ElectricityOnly',
        2 => 'GasOnly',
        4 => 'AllInOne',
        5 => 'InteractieveTV',
        8 => 'DigitalTelevision'
    ];

    public function getProducts(int $conversionPathGroupId, string $html)
    {
        $parser = $this->conversionPathGroupIdParserMap[$conversionPathGroupId] . "Parser";
        $parser = new $parser($html);

        return $parser->getProducts();
    }
}
