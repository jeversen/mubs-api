<?php

namespace ZeelandNet\MUBS\Services;

use PHPHtmlParser\Dom;

class TokenService
{
    private $mubsUrl = MUBS_BASE_URL . "/bestellen/stap1";
    /*
        @var  GuzzleHttp\Client $client
    */
    private $client;
    private $html;
    private $dom;

    public function __construct(\GuzzleHttp\Client $client)
    {
        $this->client = $client;
        $this->requestHtml();
    }

    private function requestHtml()
    {
        $this->html = $this->client->request('GET', $this->mubsUrl)->getBody();
        $this->dom =  new Dom();
        $this->dom->load($this->html);
    }

    public function getProductGroupToken() : string
    {
        return $this->getTokenFromFormById("#start");
    }

    public function getProductDetailToken() : string
    {
        return $this->getTokenFromFormById("#wizard-form");
    }

    private function getTokenFromFormById(string $formId) : string
    {
        return $this->dom->find($formId)->find("input")[0]->getAttribute("value");
    }
}
