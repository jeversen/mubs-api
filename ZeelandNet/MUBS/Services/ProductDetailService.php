<?php

namespace ZeelandNet\MUBS\Services;

use PHPHtmlParser\Dom;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;
use ZeelandNet\MUBS\ProductParsers;

class ProductDetailService
{
    private $productDetailServiceUrl = MUBS_BASE_URL . "/api/aanbod/klik-pakketgroep";
    
    private $productDetailFormData = [
        "__RequestVerificationToken" => null,
        "AddressId" => null,
        "CountryName" => null,
        "IsoNumber" => null,
        "CountryCode" => null,
        "City" => null,
        "StepNumber" => 1,
        "ZipCode" =>  null,
        "HouseNumber" => null,
        "HouseNumberAddition" => "",
        "IsHomeOrWorkAddress" => true,
        "SimulationDateEnergy" => "06-04-2018",
        "SimulationDateMultiMedia" => "06-04-2018",
        "CustomerAllreadyLivesAtAddress" => true,
        "OpenConversionPathGroup" => "",
        "wizard" => "",
        "StepType" => "Delta.Web.Portals.Mubs.UI.Models.Wizard.Step1ViewModel",
        "conversionPathGroupId" => null,
        "StreetName" => null
    ];

    private $client;
    private $tokenService;

    private $conversionPathGroupIdParserMap =
    [
        1 => 'ElectricityOnly',
        2 => 'GasOnly',
        4 => 'AllInOne',
        5 => 'InteractieveTV',
        8 => 'DigitalTelevision'
    ];

    public function __construct(\GuzzleHttp\Client $client, TokenService $tokenService)
    {
        $this->client = $client;
        $this->tokenService = $tokenService;
    }

    private function prepareFormData(int $conversionPathGroupId, array $addressInfo)
    {
        $this->productDetailFormData['__RequestVerificationToken'] = $this->tokenService->getProductDetailToken();
        $this->productDetailFormData['conversionPathGroupId'] = $conversionPathGroupId;
        
        $this->productDetailFormData['AddressId'] = $addressInfo['id'];
        $this->productDetailFormData['CountryName'] = $addressInfo['country'];
        $this->productDetailFormData['IsoNumber'] = $addressInfo['isoNumber'];
        $this->productDetailFormData['CountryCode'] = $addressInfo['countryCode'];
        $this->productDetailFormData['City'] = $addressInfo['city'];
        $this->productDetailFormData['ZipCode'] = $addressInfo['zipCode'];
        $this->productDetailFormData['HouseNumber'] = $addressInfo['houseNumber'];
        $this->productDetailFormData['HouseNumberAddition'] =  $addressInfo['houseNumberSuffix'];
        $this->productDetailFormData['StreetName'] = $addressInfo['streetname'];
    }

    public function getProductDetailsForConversionPathGroupId(int $conversionPathGroupId, array $addressInfo)
    {
        $this->prepareFormData($conversionPathGroupId, $addressInfo);
        $jsonFromWebservice  = $this->makeServiceCallAndReturnJsonResult();
        $objectFromWebservice = json_decode($jsonFromWebservice);
        $productDetailHtml = $objectFromWebservice->serviceAvailabilityHtmlResult;

        return ['products' => $this->getProductDetailsFromHtml($conversionPathGroupId, $productDetailHtml, $addressInfo) ];
    }

    private function makeServiceCallAndReturnJsonResult() : string
    {
        return $this->client
            ->request("POST", $this->productDetailServiceUrl, [ "form_params"   => $this->productDetailFormData ])
            ->getBody();
    }

    private function getProductDetailsFromHtml(int $conversionPathGroupId, string $html, array $addressInfo) : array
    {
        $parser = "\\ZeelandNet\\MUBS\\ProductParsers\\" . $this->conversionPathGroupIdParserMap[$conversionPathGroupId] . "Parser";
        $parser = new $parser($html, $addressInfo);

        return $parser->getProducts();
    }
}
