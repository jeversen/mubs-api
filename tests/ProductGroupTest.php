<?php

use PHPUnit\Framework\TestCase;
use ZeelandNet\MUBS\API;
use ZeelandNet\MUBS\Services\TokenService;

class ProductGroupTest extends TestCase
{
    public function testItGetsTheProductGroupsForJoris()
    {
        $api = new API();
        $productGroups = $api->getProductGroups("4424DA", 27, null);
        $this->assertGreaterThan(4, count($productGroups['productGroups']));
        $this->assertEquals(5695978407, $productGroups['addressInfo']['id']);
        $this->assertEquals("Wemeldinge", $productGroups['addressInfo']['city']);
    }

    public function testItGetsTheProductGroupsForMarco()
    {
        $api = new API();
        $productGroups = $api->getProductGroups("4322CG", 17, null);
        $this->assertGreaterThan(4, count($productGroups['productGroups']));
    }

    public function testItGetsTheProductGroupsForMartin()
    {
        $api = new API();
        $productGroups = $api->getProductGroups("4352AP", 7, null);
        $this->assertGreaterThan(4, count($productGroups['productGroups']));
    }

    public function testItGetsTheProductGroupsForAddressWithAddition()
    {
        $api = new API();
        $productGroups = $api->getProductGroups("4424AG", 20, "A");
        $this->assertGreaterThan(4, count($productGroups['productGroups']));
    }

    public function testItGetsTheProductGroupsForJorisAsJSON()
    {
        $api = new API();
        $productGroups = $api->getProductGroupsAsJSON("4424DA", 27, null);
        
        $this->assertJson($productGroups);
    }
}
