<?php

use PHPUnit\Framework\TestCase;
use ZeelandNet\MUBS\API;
use ZeelandNet\MUBS\Services\TokenService;

class ProductDetailTest extends TestCase
{
    public function testItGetsTheProductDetailsForJoris()
    {
        $api = new API();
        $productGroups  = $api->getProductGroups("4424DA", 27, null);
        $productDetails = $api->getProductDetails(4, $productGroups['addressInfo']);
        
        $this->assertGreaterThan(2, count($productDetails['products']));
        $this->assertEquals(10, count($productDetails['products']));
    }
}
