<?php

use PHPUnit\Framework\TestCase;
use ZeelandNet\MUBS\API;
use ZeelandNet\MUBS\Services\TokenService;

class ApiTest extends TestCase
{
    public function testInstantiateAPI()
    {
        $api = new API();
        $this->assertInstanceOf(API::class, $api);
    }

    public function testItGetsACSRFToken()
    {
        $tokenService = new TokenService(new \GuzzleHttp\Client(['cookies' => true,  /*'proxy' => "localhost:8888",*/ 'verify' => false]));
        $token = $tokenService->getProductGroupToken();
        $this->assertNotEmpty($token);
    }
}
